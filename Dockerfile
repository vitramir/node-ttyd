FROM node:12

RUN apt-get update && apt-get -y install tmux vim locales-all && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
COPY ./ttyd_linux.x86_64 /bin/ttyd
RUN chmod +x /bin/ttyd