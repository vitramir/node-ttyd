## Description

Docker image based on node:12 with ttyd and tmux installed

## Usage

```bash
docker run -p 7681:7681 node-ttyd ttyd tmux new -A -s ttyd bash
```

You can replace `bash` with command like `node` or `nodemon` up to yur case
